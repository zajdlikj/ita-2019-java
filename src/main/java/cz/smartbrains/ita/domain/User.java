package cz.smartbrains.ita.domain;

import cz.smartbrains.ita.model.UserResponseDto;
import lombok.Data;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

import javax.persistence.*;
import java.time.LocalDateTime;

@EnableAutoConfiguration
@Entity
@Table(name = "user_account")
@Data
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;


    private String firstName;

    @Column(name = "surname")
    private String lastName;

    private String email;
    private String phone;
    private String login;
    private LocalDateTime createdAt;

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFirstName()
    {
        return this.firstName;
    }
    public String getLastName()
    {
        return this.lastName;
    }
    public String getEmail()
    {
        return this.email;
    }
    public String getPhone()
    {
        return this.phone;
    }
    public String getLogin()
    {
        return this.login;
    }
    public LocalDateTime getCreatedAt() { return this.createdAt; }

}
