package cz.smartbrains.ita.service;

import cz.smartbrains.ita.domain.User;
import cz.smartbrains.ita.model.UserResponseDto;

import java.util.List;

public interface UserService {
    List<User> getUser();
//    User setUser(String login);
    UserResponseDto saveUser(User user);
    List<UserResponseDto> getAllUsers();

    List<UserResponseDto> updateUser(User user);

    List<UserResponseDto> findAllByLogin(String login);

    List<UserResponseDto> deleteUser(String login);
}
