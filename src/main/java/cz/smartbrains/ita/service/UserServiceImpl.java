package cz.smartbrains.ita.service;

import cz.smartbrains.ita.domain.User;
import cz.smartbrains.ita.model.UserResponseDto;
import cz.smartbrains.ita.repository.UserRepository;
import lombok.Data;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@Data
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserResponseDto saveUser(User user)
    {
        return mapToDto(userRepository.save(user));
    }

    @Override
    public List<UserResponseDto> updateUser(User user)
    {
        List<UserResponseDto> users = new ArrayList<>();
        for (User userFound:
                userRepository.findByLogin(user.getLogin())) {
            userFound.setFirstName(user.getFirstName());
            userFound.setLastName(user.getLastName());
            userFound.setEmail(user.getEmail());
            userFound.setPhone(user.getPhone());
            users.add(mapToDto(userFound));
        }
        return users;
    }

    @Override
    public List<UserResponseDto> getAllUsers() {
        StreamSupport.stream(userRepository.findAll().spliterator, false)
            .map(this::mapToDto)
            .collect(Collectors.toList());
        return users;
    }

    private UserResponseDto mapToDto(User entity) {
        return new UserResponseDto(
                entity.getFirstName(),
                entity.getLastName(),
                entity.getEmail(),
                entity.getPhone(),
                entity.getLogin(),
                entity.getCreatedAt()
        );
    }

    @Override
    public List<UserResponseDto> findAllByLogin(String login) {
        List<UserResponseDto> users = new ArrayList<>();
        for (User user:
                userRepository.findByLogin(login)) {
            users.add(mapToDto(user));
        }
        return users;
    }

    @Override
    @Transactional
    public List<UserResponseDto> deleteUser(String login) {
        List<UserResponseDto> users = new ArrayList<>();
        for (User user:
                userRepository.deleteByLogin(login)) {
            users.add(mapToDto(user));
        }
        return users;
    }

    public List<User> getUser()
    {
        List<User> userCollection = List.of(
 //               new User("Karel","Novak","karel@novak.ita","712 345 678","k.novak"),
 //               new User("Pepa","Novotny","pepa@novotny.ita","912 445 678","p.novotny"),
 //               new User("Zikmund","Pátý","zikmund@paty.ita","612 145 678","z.paty")
        );
        return userCollection;
    }

    /*
    public User setUser(String login)
    {
        return new User(
                //null, null, null,null, login
        );
    }
     */
}
