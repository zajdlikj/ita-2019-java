package cz.smartbrains.ita.api;


import cz.smartbrains.ita.domain.User;
import cz.smartbrains.ita.model.UserResponseDto;
import cz.smartbrains.ita.repository.UserRepository;
import cz.smartbrains.ita.service.UserService;
import lombok.RequiredArgsConstructor;
import org.hibernate.sql.Update;
import org.springframework.web.bind.annotation.*;

import javax.persistence.PostUpdate;
import java.time.LocalDateTime;
import java.util.List;

@RestController
public class UserController {

    private UserRepository userRepository;
    private final UserService userService;
    public UserController(UserService userService)
    {
        this.userService = userService;
    }

    @GetMapping("/user")
    public List<UserResponseDto> getAllUsers()
    {
        return userService.getAllUsers();
    }

    /*
    @PostMapping("/login/{login}")
    public User setUser(@PathVariable("login") String login)
    {
        return userService.setUser(login);
    }
     */

    @PostMapping("/user")
    public UserResponseDto saveUser(@RequestBody User user) {
        return userService.saveUser(user);
    }

    @PutMapping("/user")
    public List<UserResponseDto> updateUser(@RequestBody User user) {
        return userService.updateUser(user);
    }

    @DeleteMapping("/user/{login}")
    public List<UserResponseDto> deleteUser(@PathVariable("login") String login) {
        return userService.deleteUser(login);
    }

    @GetMapping("/user/login/{login}")
    public List<UserResponseDto> findAllByLogin(@PathVariable("login") String login)
    {
        return userService.findAllByLogin(login);
    }

}
