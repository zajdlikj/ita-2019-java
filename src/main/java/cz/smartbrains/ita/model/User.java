package cz.smartbrains.ita.model;

public class User {
    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private String login;

    public User(String firstName, String lastName, String email, String phone, String login)
    {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
        this.login = login;
    }

    public String getFirstName()
    {
        return this.firstName;
    }
    public String getLastName()
    {
        return this.lastName;
    }
    public String getEmail()
    {
        return this.email;
    }
    public String getPhone()
    {
        return this.phone;
    }
    public String getLogin()
    {
        return this.login;
    }

}
